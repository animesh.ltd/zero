const Zero = require("zeromq")

function buildServer() {
    const responder = Zero.socket("rep")
    
    responder.on("message", request => {
        console.log(`Received request: [${request.toString()}]`)

        // do some "work"
        setTimeout(() => {
            // send reply back to client
            responder.send("World")
        }, 1000)
    })

    responder.bindSync("tcp://*:5555")
    console.log("Listening on 5555...")

    process.on("SIGINT", () => responder.close())
}

function buildClient() {
    const requester = Zero.socket("req")

    let x = 1;
    requester.on("message", reply => {
        console.log(`Received reply ${x}: [${reply.toString()}]`)
        x++
        if (x === 11) {
            requester.close()
            process.exit(0)
        }
    })

    requester.connect("tcp://localhost:5555")

    for (let i = 1; i <= 10; i++) {
        console.log(`Sending request ${i}...`)
        requester.send("Hello")
    }

    process.on("SIGINT", () => requester.close())
}

function start() {
    buildServer()
    buildClient()
}

module.exports = { start }