const Zero = require("zeromq")

function publish() {
    const publisher = Zero.socket("pub")
    publisher.bindSync("tcp://127.0.0.1:3000")
    console.log("Pubisher bound to port 3000")

    setInterval(() => {
        console.log("Sending a multipart message envelope")
        publisher.send(["kitty cats", "meow!"])
    }, 1000)
}

function subscribe() {
    const subscriber = Zero.socket("sub")
    subscriber.connect("tcp://127.0.0.1:3000")
    subscriber.subscribe("kitty cats")
    console.log("Subscriber connected to port 3000")

    subscriber.on("message", (topic, message) => console.log(`${topic} say ${message}`))
}

function start() {
    publish()
    subscribe()
}

module.exports = { start }