const Zero = require("zeromq")

// Producer pushes information onto a socket
function startProducer() {
    const push = Zero.socket("push")
    push.bindSync("tcp://127.0.0.1:3000")
    console.log("Producer bound to port 3000")

    setInterval(() => {
        const messageId = Math.ceil(Math.random() * 1000)
        console.log("Producer: Sending message...")
        push.send(`Message #${messageId}`)
    }, 500)
}

// Worker pulls information from the socket
function startWorker() {
    const pull = Zero.socket("pull")
    pull.connect("tcp://127.0.0.1:3000")
    console.log("Worker connected to port 3000")

    pull.on("message", message => console.log("Worker: %s", message.toString()))
}

function start() {
    startProducer()
    startWorker()
}

module.exports = { start }