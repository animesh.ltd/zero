# ZeroMQ

ZeroMQ is a high-performance, asynchronous messaging library. It provides a message queue, but unlike
message-oriented middleware, a ZeroMQ system can run without a dedicated message broker. 

It is an efficient, embeddable library that solves most of the problems an application needs to become 
nicely elastic across a network, without much cost.

Specifically:

- It handles I/O asynchronously, in background threads. These communicate with application threads using 
    lock-free data structures, so concurrent ZeroMQ applications need no locks, semaphores, or other wait 
    states.
- Components can come and go dynamically and ZeroMQ will automatically reconnect. This means you can start 
    components in any order. You can create "service-oriented architectures" (SOAs) where services can join 
    and leave the network at any time.
- It queues messages automatically when needed. It does this intelligently, pushing messages as close as 
    possible to the receiver before queuing them.
- It has ways of dealing with over-full queues (called "high water mark"). When a queue is full, ZeroMQ 
    automatically blocks senders, or throws away messages, depending on the kind of messaging you are doing 
    (the so-called "pattern").
- It lets your applications talk to each other over arbitrary transports: TCP, multicast, in-process, 
    inter-process. You don't need to change your code to use a different transport.
- It handles slow/blocked readers safely, using different strategies that depend on the messaging pattern.
- It lets you route messages using a variety of patterns such as request-reply and pub-sub. These patterns 
    are how you create the topology, the structure of your network.
- It lets you create proxies to queue, forward, or capture messages with a single call. Proxies can reduce 
    the interconnection complexity of a network.
- It delivers whole messages exactly as they were sent, using a simple framing on the wire. If you write a 
    10k message, you will receive a 10k message.
- It does not impose any format on messages. They are blobs from zero to gigabytes large. When you want to 
    represent data you choose some other product on top, such as msgpack, Google's protocol buffers, and others.
- It handles network errors intelligently, by retrying automatically in cases where it makes sense.
- It reduces your carbon footprint. Doing more with less CPU means your boxes use less power, and you can 
    keep your old boxes in use for longer. Al Gore would love ZeroMQ.